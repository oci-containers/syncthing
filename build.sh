#!/usr/bin/env bash
# shellcheck disable=SC2250

set -Eeuxo pipefail

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

HOST_ARCH="$(buildah info | jq '.host.arch' | tr -d '"')"
# ARCHS="386 amd64 arm arm64 ppc64le s390x"
ARCHS=${ARCHS:-$HOST_ARCH}

MANIFEST_NAME=${MANIFEST_NAME:-syncthing}
WORKDIR=${WORKDIR:-/src}
GOVERSION=${GOVERSION:-latest}

# shellcheck disable=SC2154
if [[ ${USE_TMPFS:-false} == true ]]; then
    tmp_dir="$(mktemp -d)"
    pushd "$tmp_dir"
fi

# shellcheck disable=SC2154
if [[ ${GITLAB_CI:-false} != true ]]; then
    git clone https://github.com/syncthing/syncthing
    if [[ -v PROJECT_REF ]]; then
        pushd syncthing
        git checkout "$PROJECT_REF"
        popd
    fi

    buildah manifest rm "$MANIFEST_NAME" 2>/dev/null || true
    buildah manifest create "$MANIFEST_NAME"

    source "${SCRIPTPATH}/go_cache.sh"
fi



for ARCH in $ARCHS; do
    ctr_builder=$(buildah from "docker.io/library/golang:${GOVERSION}")
    buildah copy "$ctr_builder" syncthing "$WORKDIR"
    buildah copy "$ctr_builder" go /go
    buildah run --workingdir "$WORKDIR" --env GOARCH="$ARCH" --env CGO_ENABLED=0 --env BUILD_HOST=oci-containers --env BUILD_USER=buildah "$ctr_builder" -- /bin/sh -c "rm -f syncthing && go run build.go -no-upgrade build syncthing"


    ctr=$(buildah from --arch "$ARCH" alpine)

    buildah run "$ctr" -- apk add --no-cache ca-certificates curl libcap socat su-exec tzdata

    buildah copy --from "$ctr_builder" "$ctr" "${WORKDIR}/syncthing" /bin/syncthing
    buildah copy --from "$ctr_builder" "$ctr" "${WORKDIR}/script/docker-entrypoint.sh" /bin/entrypoint.sh
    buildah copy "$ctr" "${SCRIPTPATH}/podman-health-notifier" /bin/podman-health-notifier

    buildah run "$ctr" -- sed -i '/^set -eu$/a exec /bin/podman-health-notifier &' /bin/entrypoint.sh

    buildah config --arch "$ARCH" --label maintainer="dev@shdwchn.io" \
                   --env PUID=1000 --env PGID=1000 --env HOME=/var/syncthing --env STGUIADDRESS=0.0.0.0:8384 \
                   --port 8384 --port 22000/tcp --port 22000/udp --port 21027/udp \
                   --volume "/var/syncthing" \
                   --entrypoint '/bin/entrypoint.sh /bin/syncthing -home /var/syncthing/config' "$ctr"


    if [[ ${GITLAB_CI:-false} != true ]]; then
        buildah commit --manifest "$MANIFEST_NAME" "$ctr"
    else
        buildah commit "$ctr" "${MANIFEST_NAME}-${ARCH}"
    fi

    buildah rm "$ctr_builder"
    buildah rm "$ctr"
done



if [[ ${USE_TMPFS:-false} == true ]]; then
    popd
fi
