#!/usr/bin/env bash
# shellcheck disable=SC2250

set -Eeuxo pipefail

WORKDIR=${WORKDIR:-/src}
GOVERSION=${GOVERSION:-latest}

ctr_cache_downloader=$(buildah from "docker.io/library/golang:${GOVERSION}")
buildah run "$ctr_cache_downloader" -- mkdir "$WORKDIR"
buildah copy "$ctr_cache_downloader" syncthing/go.mod "$WORKDIR"
buildah copy "$ctr_cache_downloader" syncthing/go.sum "$WORKDIR"
buildah run --workingdir "$WORKDIR" "$ctr_cache_downloader" -- go mod download
if [[ $UID -ne 0 ]]; then
    buildah unshare sh -c "cp -r \"\$(buildah mount $ctr_cache_downloader)/go\" go" && buildah unmount "$ctr_cache_downloader"
else
    mountpoint="$(buildah mount "$ctr_cache_downloader")"
    cp -r "${mountpoint}/go" go && buildah unmount "$ctr_cache_downloader"
fi
buildah rm "$ctr_cache_downloader"

