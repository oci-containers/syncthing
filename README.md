# Unofficial syncthing OCI container
## About
This repo is used to build [syncthing](https://github.com/syncthing/syncthing) OCI container with sdnotify support for podman automatic updates and rollback. You can find built containers on [quay](https://quay.io/repository/oci-containers/syncthing). Following architectures supported: i386, amd64, armv7, aarch64, ppc64le and s390x.

Made by [shadowchain](https://gitlab.com/shdwchn10). Licensed under [MIT License](/LICENSE).

## How to use
1. Pull the container image from the user on which it will be run: `podman pull quay.io/oci-containers/syncthing:latest`.
1. Place [syncthing-podman.service](/syncthing-podman.service) file to `~/.config/systemd/user/` if you want rootless container or to `/etc/systemd/system/` if you want run container as root.
1. Change `CHANGE_VOLUME` in the service file to your host volume (where the syncthing data is stored).
1. Change `CHANGE_HOSTNAME` in the service file to a preferable container hostname.
1. Change `--network=host` in the service file to `-p 8384:8384 -p 22000:22000/tcp -p 22000:22000/udp -p 21027:21027/udp` if you don't need local device discovery and you want more security.
1. Reload systemd: `systemctl --user daemon-reload` for a rootless container or `systemctl daemon-reload` for a rootful container.
1. After that you can enable and run the container: `systemctl --user enable --now syncthing-podman.service` or `systemctl enable --now syncthing-podman.service` respectively.
1. If you want to enable automatic updates: `systemctl --user enable --now podman-auto-update.timer` or `systemctl enable --now podman-auto-update.timer`. If the automatic update fails, podman will rollback the problematic update.

## How to build locally
### Install dependencies
**Fedora Silverblue** already has required packages.

**Fedora/RHEL**:
```
sudo dnf install buildah git
```

**Ubuntu/Debian**:
```
sudo apt install buildah git
```

**Arch Linux**:
```
sudo pacman -S buildah git
```

### Build
Just run [build.sh](/build.sh). You can set some environment variables to change script behavior:

- `ARCHS` — build target architectures. If `ARCHS` is not set, the script builds container image **only for the host CPU architecture**.
- Set `USE_TMPFS` to `true` to build in `/tmp`. Otherwise the script will build the image **in the current directory**.

Example:
```
USE_TMPFS=true ARCHS="386 amd64 arm arm64 ppc64le s390x" ./build.sh
```

## TODO
- Container CI testing
- More DRY
- Maybe more container tags on quay
- Find an easy way to automagically trigger CI on new syncthing releases
